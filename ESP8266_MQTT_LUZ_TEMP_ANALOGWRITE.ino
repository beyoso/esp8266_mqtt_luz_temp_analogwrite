/*
  Basic ESP8266 MQTT Rele example

  

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
#include "DHT.h"

DHT dht;

#define MOSFET D5
#define DHT D2

#define BUILTIN_LED 2 //ledBuiltin lolin Nodemcu v3

// establecemos los parametros para tener acceso a la red
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.15.100";
const int mqtt_port= 1883;
const char* topicLuz = "campo/patio/luz";
const char* topicTemp = "campo/patio/temperatura";
const char* topicHum = "campo/patio/humedad";
const char* topicGuidnarla = "campo/patio/guidnarla";
const char* nameNode = "NodoPatio";
int periodo = 5000;
unsigned long tiempo;
//dht
int varT = 0;
int varH = 0;
//luz
const long A = 10000;     //Resistencia en oscuridad en KΩ
const int B = 100;        //Resistencia a la luz (10 Lux) en KΩ
const int Rc = 10;       //Resistencia calibracion en KΩ
const int LDRPin = A0;   //Pin del LDR
int varLuz = 0;
int ilum = 0;
byte luzAc = 0; 
WiFiClient espClient;
PubSubClient client(espClient);



void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(MOSFET,OUTPUT);
  digitalWrite(BUILTIN_LED, 1);
  setPowerMosfet(0);
  dht.setup(D2); // data pin D2
  Serial.begin(115200);
  Setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
   
}

void Setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
void sendHumAndTemp(int humOld, int tempOld){
    
    delay(dht.getMinimumSamplingPeriod());

    float h = dht.getHumidity();
    float t = dht.getTemperature();

    Serial.print(dht.getStatusString());
    Serial.print("\t");
    Serial.print(h, 1);
    Serial.print("\t\t");
    Serial.print(t, 1);
    Serial.print("\t\t");
    if(h == humOld && t == tempOld){
      return;
      }
    varH= (int)h;
    varT=(int)t;  
    String temperature = String(t);
    String humidity = String(h);
    //SendLuz
 
    String payload ;
    payload ="{";payload += "\"temperature\":"; payload += temperature; payload += "}";
    char attributes[200];
  
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
        if (client.publish( topicTemp, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
    payload  ="{";payload += "\"humidity\":"; payload += humidity; payload += "}";
    char attributes2[200];
  
    payload.toCharArray( attributes2, 200 );
    Serial.println( attributes2 );
        if (client.publish( topicHum, attributes2 ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
  
}
void setPowerMosfet(int p){
 
  analogWrite(MOSFET, map(p,0,100,1023,0));
  }
int sendLuz( int luzOld){

    //SendLuz
    //int getLuz = map(analogRead(A0),1023,0,0,100);
    int V = analogRead(A0); 
    // ilum = ((long)(1024-V)*A*10)/((long)B*Rc*V);  //usar si LDR entre GND y A0 
    //ilum = ((long)V*A*10)/((long)B*Rc*(1024-V));    //usar si LDR entre A0 y Vcc (como en el esquema anterior)
    Serial.print("\n\n\n\nValor A0-->");Serial.println(V);
    
    //Serial.print("Valor ilum--------->");Serial.println(ilum); 
    luzAc = map(V,1023,0,0,100);
    Serial.print("Valor getLuz--------->");Serial.println(luzAc); 
    Serial.print("\n\n\n\n");
    if(luzAc == luzOld){return luzAc ;}
    String payload = "{";
    payload += "\"luz\":"; payload += luzAc; 
    payload += "}";
    
    // Send payload
    char attributes[200];
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
    if (client.publish( topicLuz, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }

  return luzAc;
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message recibido [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  //JSON
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  
 
  int power = doc["power"];
  Serial.print("power: ");Serial.println(power);
  setPowerMosfet(power);

}

void reconnect() {
  if(!WiFi.isConnected()){Setup_wifi();}
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(topicLuz, nameNode);
     client.publish(topicHum, nameNode);
     client.publish(topicTemp, nameNode);
      // ... and resubscribe
      client.subscribe(topicGuidnarla);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  if(millis() > tiempo + periodo){
        tiempo = millis();
        varLuz = sendLuz(varLuz);
        sendHumAndTemp(varH,varT);
        
    }
    delay.yield();
}
