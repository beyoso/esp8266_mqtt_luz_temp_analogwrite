/*
  Basic ESP8266 MQTT Rele example

*/
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>//V6.03
#include "DHT.h"
#include <Configuration.hpp>
#include <eeprom.hpp>


DHT dht;

#define MOSFET 14//D5 = 14
#define PinDHT 4 //D2 = 4
int PinPresencia = D8;
int presenciaOld = 0;

#define BUILTIN_LED 2 //ledBuiltin lolin Nodemcu v3


int periodo = 10000;
unsigned long tiempo;
unsigned long tiempo2;
//dht
int varT = 0;
int varH = 0;
//luz

const int LDRPin = A0;   //Pin del LDR
float luzOld = 0;
int cont = 0;
float luzAc = 0; 
float humAc = 0;
float temAc = 0;
float humOld = 0;
float temOld = 0;
int contDht = 0;
WiFiClient espClient;
PubSubClient client(espClient);

int activacion = 0;
eeprom e;
unsigned long tiempoDelay;
unsigned long tiempoActivacion;

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(MOSFET,OUTPUT);
  pinMode(PinPresencia,INPUT);
  digitalWrite(BUILTIN_LED, 1);
  setPowerMosfet(0);
  dht.setup(PinDHT); // data pin D2
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  e.load();
  tiempoDelay = millis();
   
}

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void sendHumAndTemp(int hum, int temp)
{

    String payload ;
    payload ="{";payload += "\"temperature\":"; payload += temp; payload += "}";
    char attributes[200];
  
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
        if (client.publish( topicTemp, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
    payload  ="{";payload += "\"humidity\":"; payload += hum; payload += "}";
    char attributes2[200];
  
    payload.toCharArray( attributes2, 200 );
    Serial.println( attributes2 );
        if (client.publish( topicHum, attributes2 ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }
  
}
void setPowerMosfet(int p)
{
  analogWrite(MOSFET, map(p,0,100,1023,0));
  }
int sendLuz(float luzAc){

    String payload = "{";
    payload += "\"luz\":"; payload += luzAc; 
    payload += "}";
    
    // Send payload
    char attributes[200];
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
    if (client.publish( topicLuz, attributes ) == true) {
        Serial.println("Success sending message");
    } else {
        Serial.println("Error sending message");
    }

  return luzAc;
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message recibido [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  //JSON
  StaticJsonDocument<200> doc;
  DeserializationError error = deserializeJson(doc, payload);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  
  //{"power":60,"delay":10,"tActivacion":20}
  int power = doc["power"];
  Serial.print("power recibido: ");Serial.println(power);
  setPowerMosfet(power);
  char delayAux = doc["delay"];
  Serial.print("delay recibido: ");Serial.println(delay);
  char tActiAux = doc["tActivacion"];
  Serial.print("tiempo Activacion recibido: ");Serial.println(tActiAux);
  if(delayAux != 0 && tActiAux != 0)
    e.SetEeprom(delayAux,tActiAux);
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(nameNode)) {
      Serial.println("connected");
      // Enviamos los datos obtenidos
     client.publish(topicLuz, nameNode);
     client.publish(topicHum, nameNode);
     client.publish(topicTemp, nameNode);
      // ... and resubscribe
      client.subscribe(topicSetValue);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
int readLuz()
{
    //SendLuz
    //int getLuz = map(analogRead(A0),1023,0,0,100);
    int V = analogRead(A0); 
    
    //Serial.print("Valor ilum--------->");Serial.println(ilum); 
    
    return V;

}
void sendPresencia(bool b)
{
  Serial.println("Precencia detectada");
    String payload ;
    payload ="{";payload += "\"presencia\":";
  if(b){
     
     payload += 1; payload += "}";
     
    }
    else{
      
      payload += 0; payload += "}";
    
    }
    char attributes[200];
  
    payload.toCharArray( attributes, 200 );
    Serial.println( attributes );
    client.publish( topicpresencia, attributes );

}
void loop() {
  if(!WiFi.isConnected()){setup_wifi();}
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  #region Presencia
  int presenciaAc = digitalRead(PinPresencia);
  if(presenciaAc!= presenciaOld && (millis()>tiempoDelay +e.GetDelay()*1000)) 
  {
    tiempoDelay = millis();
    presenciaOld = presenciaAc;
    sendPresencia(presenciaAc);

  }
  if(presenciaOld==HIGH){setPowerMosfet(100); tiempoActivacion = millis();}
  if((millis()>tiempoActivacion +e.GetTiempoActivacion()*1000))
  {
    setPowerMosfet(0);
  }
  #pragma endregion
  //LUZ
  if(millis()>tiempo2 +1000)
  {
    if(presenciaOld==1){setPowerMosfet(100);}else{map((int)luzAc,1023,0,0,100);}
    tiempo2 = millis();
    luzAc += analogRead(A0); 
    cont++;
    delay(dht.getMinimumSamplingPeriod());
    float h = dht.getHumidity();
    float t = dht.getTemperature();
    if(!(isnan(h)|| isnan(t)))
    {
      humAc += (int)h;
      temAc += (int)t;
      contDht++;
    }
    
  }
  //DHT 
  if(millis() > tiempo + periodo){
      tiempo = millis();
      //calculo luz Ac
      luzAc =  (luzAc/cont);
      luzAc = map((int)luzAc,1023,0,0,100);
      Serial.print("Valor media luz--------->");Serial.println(luzAc); 

      if(luzOld != luzAc)
      {
        luzOld = luzAc;
        sendLuz(luzAc);
        
      }
      humAc = (int)(humAc/contDht);
      temAc = (int)(temAc/contDht);
      if(temAc != temOld || humAc != humOld )
      {
        temOld = temAc;
        humOld = humAc; 
        sendHumAndTemp(humAc,temAc);
      }
      cont = 0;
      luzAc = 0;
      humAc = 0;
      temAc =0;
      contDht = 0;
      

    }
    yield();
}
