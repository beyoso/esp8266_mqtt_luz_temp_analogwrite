#include <Arduino.h>
#include <EEPROM.h>



class eeprom
{
    private:
        char DELAY =0;
        char T_ACTIVACIOM = 0;
    public:
        
        void load()
        {
            EEPROM.begin(512);
            DELAY= EEPROM.read(0);
            T_ACTIVACIOM = EEPROM.read(1);
        }
        bool SetEeprom(char delay, char t_activacion)
        {
            if(delay != DELAY)
            EEPROM.write(0,delay);
            if(t_activacion != T_ACTIVACIOM)
                EEPROM.write(1,t_activacion);
            if (EEPROM.commit()) {
                return true;
            } else {
                return false;
            }
        }
        char GetDelay(){return DELAY;}
        char GetTiempoActivacion(){return T_ACTIVACIOM;}
};
