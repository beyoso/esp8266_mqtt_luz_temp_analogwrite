// establecemos los parametros para tener acceso a la red
const char* ssid = "vereda-R";
const char* password = "vereda2019";
const char* mqtt_server = "192.168.1.100";
const int mqtt_port= 1883;
const char* topicLuz = "campo/patio/luz";
const char* topicTemp = "campo/patio/temperatura";
const char* topicHum = "campo/patio/humedad";
const char* topicpresencia = "campo/patio/presencia";
const char* topicSetValue = "campo/patio/setValue";
const char* nameNode = "NodoPatio";

void setup();
void setup_wifi();
void sendHumAndTemp(int hum, int temp);
void setPowerMosfet(int p);
int sendLuz(float luzAc);
void callback(char* topic, byte* payload, unsigned int length);
void reconnect();
int readLuz();
void loop();